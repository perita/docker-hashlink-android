LOCAL_PATH := $(call my-dir)

###########################
#
# Hashlink shared library
#
###########################

include $(CLEAR_VARS)

LOCAL_MODULE := hashlink

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/src \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/pcre \
	$(LOCAL_PATH)/include/minimp3 \
	$(LOCAL_PATH)/include/mikktspace \
	$(LOCAL_PATH)/../SDL/include \

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/src

LOCAL_SRC_FILES := \
	$(subst $(LOCAL_PATH)/,, \
	$(wildcard $(LOCAL_PATH)/include/pcre/*.c) \
	$(LOCAL_PATH)/src/std/array.c \
	$(LOCAL_PATH)/src/std/buffer.c \
	$(LOCAL_PATH)/src/std/bytes.c \
	$(LOCAL_PATH)/src/std/cast.c \
	$(LOCAL_PATH)/src/std/date.c \
	$(LOCAL_PATH)/src/std/error.c \
	$(LOCAL_PATH)/src/std/file.c \
	$(LOCAL_PATH)/src/std/fun.c \
	$(LOCAL_PATH)/src/std/maps.c \
	$(LOCAL_PATH)/src/std/math.c \
	$(LOCAL_PATH)/src/std/obj.c \
	$(LOCAL_PATH)/src/std/random.c \
	$(LOCAL_PATH)/src/std/regexp.c \
	$(LOCAL_PATH)/src/std/socket.c \
	$(LOCAL_PATH)/src/std/string.c \
	$(LOCAL_PATH)/src/std/sys.c \
	$(LOCAL_PATH)/src/std/track.c \
	$(LOCAL_PATH)/src/std/types.c \
	$(LOCAL_PATH)/src/std/ucs2.c \
	$(LOCAL_PATH)/src/std/thread.c \
	$(LOCAL_PATH)/src/std/process.c \
	$(LOCAL_PATH)/src/std/sys_android.c \
	$(LOCAL_PATH)/src/gc.c \
	$(LOCAL_PATH)/libs/fmt/fmt.c \
	$(LOCAL_PATH)/libs/fmt/sha1.c \
	$(LOCAL_PATH)/libs/fmt/dxt.c \
	$(LOCAL_PATH)/libs/fmt/mikkt.c \
	$(LOCAL_PATH)/include/mikktspace/mikktspace.c \
	$(LOCAL_PATH)/libs/sdl/sdl.c \
	$(LOCAL_PATH)/libs/sdl/gl.c \
	$(LOCAL_PATH)/libs/ui/ui_stub.c \
	$(LOCAL_PATH)/libs/openal/openal.c \
	)

# Warnings we haven't fixed (yet)
LOCAL_CFLAGS += -Wno-unused-parameter -Wno-sign-compare

LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -lGLESv3 -lOpenSLES -llog -landroid

ifeq ($(NDK_DEBUG),1)
    cmd-strip :=
endif

LOCAL_SHARED_LIBRARIES := SDL2
LOCAL_STATIC_LIBRARIES := libpng libjpeg-turbo libvorbis openal

include $(BUILD_SHARED_LIBRARY)
