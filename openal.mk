LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := openal

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include $(LOCAL_PATH) $(LOCAL_PATH)/OpenAL32/Include $(LOCAL_PATH)/Alc $(LOCAL_PATH)/common

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include

LOCAL_SRC_FILES := \
	$(subst $(LOCAL_PATH)/,, \
    	$(wildcard $(LOCAL_PATH)/OpenAL32/*.c) \
	$(wildcard $(LOCAL_PATH)/Alc/*.c) \
	$(wildcard $(LOCAL_PATH)/Alc/effects/*.c) \
	$(wildcard $(LOCAL_PATH)/Alc/filters/*.c) \
	$(wildcard $(LOCAL_PATH)/common/*.c) \
	$(LOCAL_PATH)/Alc/mixer/hrtf_inc.c \
	$(LOCAL_PATH)/Alc/mixer/mixer_c.c \
	$(LOCAL_PATH)/Alc/backends/base.c \
	$(LOCAL_PATH)/Alc/backends/loopback.c \
	$(LOCAL_PATH)/Alc/backends/null.c \
	$(LOCAL_PATH)/Alc/backends/opensl.c \
	)

LOCAL_STATIC_LIBRARIES := 

LOCAL_SHARED_LIBRARIES := SDL2

LOCAL_CFLAGS := -ffast-math -DAL_BUILD_LIBRARY -DAL_ALEXT_PROTOTYPES
LOCAL_LDLIBS := -llog -lOpenSLES 

include $(BUILD_STATIC_LIBRARY)
