FROM cimg/android:2021.08-ndk

COPY 0001-libjpeg-turbo--add-bmp-and-ppm.patch /home/circleci

RUN set -eux \
	&& sudo apt-get update \
	&& sudo apt-get install ed \
	&& sudo apt-get clean \
	&& cd /home/circleci \
	&& mkdir SDL \
	&& cd SDL \
	&& git init \
	&& git remote add origin https://github.com/libsdl-org/SDL.git \
	&& git fetch --depth 1 origin d9d84c8d731ff137453263409c56ba5bad4c8940 \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& cd /home/circleci \
	&& mkdir hashlink \
	&& cd hashlink \
	&& git init \
	&& git remote add origin https://github.com/HaxeFoundation/hashlink.git \
	&& git fetch --depth 1 origin 2be3f7a9e633cd8f820a632c8517f72ec6db75e0 \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& cd /home/circleci \
	&& mkdir libpng \
	&& cd libpng \
	&& git init \
	&& git remote add origin https://github.com/julienr/libpng-android.git \
	&& git fetch --depth 1 origin 8bf331e403dac32ab3652a1ef457d2ea7ab4660a \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& cd /home/circleci \
	&& mkdir libjpeg-turbo \
	&& cd libjpeg-turbo \
	&& git init \
	&& git remote add origin https://github.com/DeviceFarmer/android-libjpeg-turbo \
	&& git fetch --depth 1 origin aa06cf0972d10b72a95e89a20138fcff980b7dff \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& patch -p1 < /home/circleci/0001-libjpeg-turbo--add-bmp-and-ppm.patch \
	&& rm /home/circleci/0001-libjpeg-turbo--add-bmp-and-ppm.patch \
	&& cd /home/circleci \
	&& mkdir vorbis \
	&& cd vorbis \
	&& git init \
	&& git remote add origin https://github.com/xiph/vorbis.git \
	&& git fetch --depth 1 origin 0657aee69dec8508a0011f47f3b69d7538e9d262 \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& cd /home/circleci \
	&& mkdir ogg \
	&& cd ogg \
	&& git init \
	&& git remote add origin https://github.com/xiph/ogg.git \
	&& git fetch --depth 1 origin e1774cd77f471443541596e09078e78fdc342e4f \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& cd /home/circleci \
	&& mkdir openal \
	&& cd openal \
	&& git init \
	&& git remote add origin https://github.com/kcat/openal-soft.git \
	&& git fetch --depth 1 origin 1226aec2fcf1a10d60be52205a22e538d80108a3 \
	&& git checkout FETCH_HEAD \
	&& rm -fr .git \
	&& cd /home/circleci/openal \
	&& gcc -o bsincgen native-tools/bsincgen.c -lm \
        && ./bsincgen bsinc_inc.h \
	&& gcc -o bin2h native-tools/bin2h.c \
	&& ./bin2h hrtf/default-44100.mhr default-44100.mhr.h hrtf_default_44100 \
	&& ./bin2h hrtf/default-48000.mhr default-48000.mhr.h hrtf_default_48000

COPY hashlink.mk /home/circleci/hashlink/Android.mk
COPY vorbis.mk /home/circleci/vorbis/Android.mk
COPY openal.mk /home/circleci/openal/Android.mk
COPY config.h version.h /home/circleci/openal/
COPY ogg.mk /home/circleci/ogg/Android.mk
COPY config_types.h /home/circleci/ogg/include/ogg
COPY app.mk /home/circleci

RUN set -eux \
	&& /home/circleci/SDL/build-scripts/androidbuild.sh org.libsdl.testgles /home/circleci/SDL/test/testgles.c \
	&& cd /home/circleci/SDL/build/org.libsdl.testgles/app/jni \
	&& sed -i -e 's/android-16/android-21/g' Application.mk \
	&& ln -s /home/circleci/hashlink \
	&& ln -s /home/circleci/libjpeg-turbo \
	&& ln -s /home/circleci/libpng/jni libpng \
	&& ln -s /home/circleci/ogg \
	&& ln -s /home/circleci/openal \
	&& ln -s /home/circleci/vorbis \
	&& cd /home/circleci/SDL/build/org.libsdl.testgles \
	&& sed -i -e 's/minSdkVersion 16/minSdkVersion 21/g' -e 's/android-16/android-21/g' app/build.gradle \
	&& sed -i -e 's/glEsVersion="0x00020000"/glEsVersion="0x00030000"/g' app/src/main/AndroidManifest.xml \
	&& V=1 ./gradlew --no-daemon buildDebug \
	&& rm -fr /home/circleci/SDL/build

