LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libvorbis

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include $(LOCAL_PATH)/lib

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include

FILTER_OUT := \
	$(LOCAL_PATH)/lib/psytune.c \
	$(LOCAL_PATH)/lib/misc.c

LOCAL_SRC_FILES := \
	$(subst $(LOCAL_PATH)/,, \
	$(filter-out $(FILTER_OUT), \
	$(wildcard $(LOCAL_PATH)/../ogg/src/*.c) \
	$(wildcard $(LOCAL_PATH)/lib/*.c)))

LOCAL_STATIC_LIBRARIES := libogg

include $(BUILD_STATIC_LIBRARY)
